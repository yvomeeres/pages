My research interest is robustness from an interdisciplinary perspective.
I love to describe robust data and communication in a mathematically precise way,
from the view-point of neuroscience, cognition and artificial intelligence.

Mimicking human intelligence by robust behavior in technical systems is one of the applications 
I would like to accomplish.

[F](https://github.com/MichaelWehar/FourCornersProblem)
[very-drafty-paper](https://codeberg.org/yvomeeres/pubs/raw/branch/draft/dag-draft.pdf)
[drafty-slides](https://codeberg.org/yvomeeres/pubs/raw/branch/draft/dag-slides-draft.pdf)
