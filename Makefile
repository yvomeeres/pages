# index.html ist momentan manuell erstellt TODO
html:
	echo "running pandoc"
	pandoc --bibliography=yvomeeres.bib index.tex -o index.html
push:
	GIT_SSH_COMMAND='ssh -i ~/.ssh/yvomeeres' git push


# generation of html from bib produces wrong order:
# pandoc seems to ignore the sort option in the latex code for the bibliography
pandoc:
	pandoc yvomeeres.bib  -s --citeproc -o pub.html
	pandoc research.md -o research.html
